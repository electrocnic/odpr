'use strict'
import i18n from '@/_localization/localization'

export default {
	methods: {
		i18nJson () {
			return i18n.json()
		}
	}
}
