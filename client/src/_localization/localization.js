'use strict'
import en from './en.json'

const i18n = {
	json
}

function json () {
	return en
}

export default i18n
