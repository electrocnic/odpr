'use strict'

export const getSceneryData = 'getSceneryData'
export const resetSceneryData = 'resetSceneryData'
export const deleteScenery = 'deleteScenery'
export const reportScenery = 'reportScenery'
