'use strict'

export const getInitialSceneries = 'getInitialSceneries'
export const getNextSceneries = 'getNextSceneries'
export const resetSceneryStore = 'resetSceneryStore'
