'use strict'
export const actionCloseModal = 'closeModal'
export const actionFlipModal = 'flipModal'
export const actionSubmitPasswordChange = 'submitPasswordChange'

export default {
	actionCloseModal,
	actionFlipModal,
	actionSubmitPasswordChange
}
