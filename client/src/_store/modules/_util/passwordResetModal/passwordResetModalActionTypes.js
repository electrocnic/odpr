'use strict'
export const actionCloseModal = 'closeModal'
export const actionFlipModal = 'flipModal'
export const actionSubmitPasswordReset = 'submitPasswordReset'

export default {
	actionCloseModal,
	actionFlipModal,
	actionSubmitPasswordReset
}
