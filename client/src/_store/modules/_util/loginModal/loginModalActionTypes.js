'use strict'
export const actionCloseModal = 'closeModal'
export const actionFlipModal = 'flipModal'
export const actionSubmitLogin = 'submitLogin'
export const actionSubmitRegister = 'submitRegister'
export const actionSubmitPasswordReset = 'submitPasswordReset'

export default {
	actionCloseModal,
	actionFlipModal,
	actionSubmitLogin,
	actionSubmitRegister,
	actionSubmitPasswordReset
}
