stages:
  - update
  - build
  - test
  - release
  - deploy
  - analyze

variables:
  DOCKER_HOST: tcp://docker:2375/
  # When using dind, it's wise to use the overlayfs driver for
  # improved performance.
  DOCKER_DRIVER: overlay2
  # DOCKER_REGISTRY: docker.gitlab.electrocnic.com # (define it in gitlab variables)
  #
  # Contact
  #
  # CERTBOT_EMAIL: example@gmail.com # (define it in gitlab variables)
  #
  # Version
  #
  # Define the versions of the production images in the gitlab variables settings instead of committing them!
  # CI_BUILD_TAG will be used by default for each tag on a commit, which will trigger a tagged pipeline.
  # CI_BUILD_TAG will be used for all images (nginx, app and postgres), even if one or more of them do not have
  #  any changes to their preceding version.
  # If you want to keep track for each of those images seperately, overwrite the version per image by defining their
  #  corresponding version somewhere (example values below):
  # APP_VERSION: 1.1.5
  # NGINX_VERSION 1.0.11
  # POSTGRES_VERSION 1.0.2
  # (Using CI_BUILD_TAG, they would always be the same value across the images)
  #
  # Docker images
  #
  BASE_IMAGE: "docker:18.09.7-dind"
  DIND_IMAGE: "${DOCKER_REGISTRY}/${CI_PROJECT_PATH}/debian-dind:latest"
  BUILDER_IMAGE: "${DOCKER_REGISTRY}/${CI_PROJECT_PATH}/builder:latest"
  APP_IMAGE_DEBUG: "${DOCKER_REGISTRY}/${CI_PROJECT_PATH}/app"
  NGINX_IMAGE_DEBUG: "${DOCKER_REGISTRY}/${CI_PROJECT_PATH}/nginx"
  POSTGRES_IMAGE_DEBUG: "${DOCKER_REGISTRY}/${CI_PROJECT_PATH}/postgres"
  PRERENDER_IMAGE_DEBUG: "${DOCKER_REGISTRY}/${CI_PROJECT_PATH}/prerender"
  APP_IMAGE_PRODUCTION_TEST: "${APP_IMAGE_DEBUG}:${CI_BUILD_TAG}-test"
  NGINX_IMAGE_PRODUCTION_TEST: "${NGINX_IMAGE_DEBUG}:${CI_BUILD_TAG}-test"
  POSTGRES_IMAGE_PRODUCTION_TEST: "${POSTGRES_IMAGE_DEBUG}:${CI_BUILD_TAG}-test"
  PRERENDER_IMAGE_PRODUCTION_TEST: "${PRERENDER_IMAGE_DEBUG}:${CI_BUILD_TAG}-test"
  APP_IMAGE_PRODUCTION: "${APP_IMAGE_DEBUG}:${CI_BUILD_TAG}"
  NGINX_IMAGE_PRODUCTION: "${NGINX_IMAGE_DEBUG}:${CI_BUILD_TAG}"
  POSTGRES_IMAGE_PRODUCTION: "${POSTGRES_IMAGE_DEBUG}:${CI_BUILD_TAG}"
  PRERENDER_IMAGE_PRODUCTION: "${PRERENDER_IMAGE_DEBUG}:${CI_BUILD_TAG}"
  #
  # Directories
  #
  CLIENT_DIRECTORY: "client"
  BACKEND_DIRECTORY: "backend"
  DIND_VERSION_SCRIPTS_SRC_DIRECTORY: "debian-dind"
  DIND_VERSION_SCRIPTS_DIRECTORY: "/version"
  BUILDER_VERSION_SCRIPTS_SRC_DIRECTORY: "builder"
  BUILDER_VENV_DIRECTORY: "/.venv"
  BUILDER_VERSION_SCRIPTS_DIRECTORY: "/version"
  BUILDER_NODE_MODULES_SRC_DIRECTORY: "/node_modules"
  BUILDER_NODE_MODULES_DESTINATION_DIRECTORY: "${CI_PROJECT_DIR}/${CLIENT_DIRECTORY}/node_modules"
  BUILDER_STATIC_SRC_DIRECTORY: "${CI_PROJECT_DIR}/${CLIENT_DIRECTORY}/${STATICFILES_DIRNAME}/static"
  STATICFILES_DIRNAME: 'static'
  MEDIAFILES_DIRNAME: 'media'
  NGINX_IMAGE_STATICFILES_DESTINATION_DIRECTORY: "/${STATICFILES_DIRNAME}"
  NGINX_IMAGE_MEDIAFILES_DESTINATION_DIRECTORY: "/${MEDIAFILES_DIRNAME}"
  APP_IMAGE_VENV_DIRECTORY: "/.venv"
  APP_IMAGE_BACKEND_DIRECTORY: "/backend"
  APP_INDEX_HTML_TEMPLATE_DIRECTORY: "templates/index.html"
  #
  # Vue/Webpack Settings
  #
  VUE_DEBUG: "true" # (true or false)  (defined here and in jobs)
  #
  # Django Settings (some of them are defined in the gitlab variables page and are commented here)
  #
  # DJANGO_SECRET_KEY_DEBUG  (defined in gitlab)
  # DJANGO_SECRET_KEY_TEST  (defined in gitlab)
  # DJANGO_SECRET_KEY  (defined in gitlab)
  DJANGO_DEBUG: "True" # (True or False)  (defined here)
  #
  # Django Database settings
  #
  # DJANGO_DATABASE_NAME  (e.g. 'database1')  (defined in gitlab variables)
  # DJANGO_DATABASE_USERNAME  (defined in gitlab variables)
  # DJANGO_DATABASE_PASSWORD_DEBUG  (defined in gitlab variables)
  # DJANGO_DATABASE_PASSWORD_TEST  (defined in gitlab variables)
  # DJANGO_DATABASE_PASSWORD  (defined in gitlab variables)
  DJANGO_DATABASE_PORT: 5432
  # DJANGO_DATABASE_HOST  (must be the same name as the docker-compose service, e.g 'database1')  (defined in gitlab)
  #
  # Django allowed host domains (optional)
  #
  # DJANGO_ALLOWED_HOST_1  (defined in release-job)
  # DJANGO_ALLOWED_HOST_2  (defined in release-job)
  # DJANGO_ALLOWED_HOST_3  (define it in gitlab variables)
  # DJANGO_ALLOWED_HOST_4  (define it in gitlab variables)
  #
  # Django staticfiles settings
  #
  DJANGO_STATICFILES_RELATIVE_TO_BACKEND_DIRECTORY: "../${CLIENT_DIRECTORY}/${STATICFILES_DIRNAME}" #  (default = '../client/static-vuedj')
  DJANGO_STATIC_ROOT_RELATIVE_TO_BACKEND_DIRECTORY: "../${STATICFILES_DIRNAME}/static" #  (default = '../staticfiles/static')
  DJANGO_MEDIA_ROOT_RELATIVE_TO_BACKEND_DIRECTORY: "../media" #  (default = '../staticfiles/media')
  DJANGO_STATIC_URL: "/${STATICFILES_DIRNAME}/" #     (default = '/static-vuedj/')
  DJANGO_MEDIA_URL: "/${MEDIAFILES_DIRNAME}/" #       (default = '/media/')
  #
  # Misc
  #
  GUNICORN_PORT: 8000 # inner port of the App-Docker-Container (exposed to nginx)
  HOST_PORT: 8000 # port of gitlab-ci runner container which runs cypress to test the app on app@localhost:HOST_PORT
  # NGINX_PRODUCTION_CONFIG  (defined in release-job locally).
  # PRODUCTION_URL  (defined in release-job locally. If defined, will overwrite DEBUG_URL. Should be in the format: "https://my.site.com/" (don't forget the trailing slash))
  DEBUG_URL: "http://localhost:${GUNICORN_PORT}/"
  NGINX_SERVER_NAME: "localhost"
  # NGINX_PERMANENT_REDIRECT_INSTRUCTION: "" # (defined in release-job locally). Leave empty for debug builds.
  # NGINX_BASIC_AUTH_FOR_TEST_DOMAIN: "" # (defined in release-job locally). Leave empty for production builds.
  #
  #
  # !!! Define them in gitlab variables: !!!
  #
  # DOCKER_USER
  # DOCKER_PW
  # TEST_PRODUCTION_DOMAIN
  # TEST_PRODUCTION_DOMAIN_USER
  # TEST_PRODUCTION_DOMAIN_PASSWORD
  # PRODUCTION_DOMAIN
  # DEPLOY_SERVER_IP
  # CERTBOT_EMAIL
  # DJANGO_SECRET_KEY
  # DJANGO_DATABASE_NAME
  # DJANGO_DATABASE_HOST
  # DJANGO_DATABASE_USERNAME
  # DJANGO_DATABASE_PASSWORD
  # DJANGO_ALLOWED_HOST_3 (optional)
  # DJANGO_ALLOWED_HOST_4 (optional)
  #

services:
  - $BASE_IMAGE

update-base-image:
  stage: update
  image: $BASE_IMAGE
  before_script:
    - cd $CI_PROJECT_DIR
    - echo $DOCKER_PW | docker login -u $DOCKER_USER --password-stdin $DOCKER_REGISTRY
  script:
    - sh util/update_builder_and_dind_image.sh
  except:
    - schedules

build-images:
  stage: build
  image: $BUILDER_IMAGE
  variables:
    DJANGO_DATABASE_PASSWORD: "${DJANGO_DATABASE_PASSWORD_DEBUG}"
    DJANGO_SECRET_KEY: "${DJANGO_SECRET_KEY_DEBUG}"
  before_script:
    - cd $CI_PROJECT_DIR
    - echo $DOCKER_PW | docker login -u $DOCKER_USER --password-stdin $DOCKER_REGISTRY
    - mv "${BUILDER_NODE_MODULES_SRC_DIRECTORY}" "${BUILDER_NODE_MODULES_DESTINATION_DIRECTORY}"
  script:
    - ./build_gitlab.sh
    - docker build
      --build-arg CLIENT_DIRECTORY
      --build-arg BACKEND_DIRECTORY
      --build-arg APP_IMAGE_BACKEND_DIRECTORY
      --build-arg APP_INDEX_HTML_TEMPLATE_DIRECTORY
      --build-arg DJANGO_SECRET_KEY
      --build-arg DJANGO_DEBUG
      --build-arg DJANGO_DATABASE_NAME
      --build-arg DJANGO_DATABASE_USERNAME
      --build-arg DJANGO_DATABASE_PASSWORD
      --build-arg DJANGO_DATABASE_PORT
      --build-arg DJANGO_DATABASE_HOST
      --build-arg DJANGO_ALLOWED_HOST_1
      --build-arg DJANGO_ALLOWED_HOST_2
      --build-arg DJANGO_ALLOWED_HOST_3
      --build-arg DJANGO_ALLOWED_HOST_4
      --build-arg DJANGO_STATICFILES_RELATIVE_TO_BACKEND_DIRECTORY
      --build-arg DJANGO_STATIC_ROOT_RELATIVE_TO_BACKEND_DIRECTORY
      --build-arg DJANGO_MEDIA_ROOT_RELATIVE_TO_BACKEND_DIRECTORY
      --build-arg DJANGO_STATIC_URL
      --build-arg DJANGO_MEDIA_URL
      --build-arg GUNICORN_PORT
      -t $APP_IMAGE_DEBUG .
    - docker build
      --build-arg NGINX_IMAGE_STATICFILES_DESTINATION_DIRECTORY
      --build-arg NGINX_IMAGE_MEDIAFILES_DESTINATION_DIRECTORY
      --build-arg CERTBOT_EMAIL
      --build-arg GUNICORN_PORT
      --build-arg NGINX_SERVER_NAME
      -t $NGINX_IMAGE_DEBUG -f nginx/Dockerfile .
    - docker build
      --build-arg DJANGO_DATABASE_NAME
      --build-arg DJANGO_DATABASE_USERNAME
      --build-arg DJANGO_DATABASE_PASSWORD
      -t $POSTGRES_IMAGE_DEBUG -f postgres/Dockerfile .
    - docker build
      -t $PRERENDER_IMAGE_DEBUG -f prerender/Dockerfile .
  after_script:
    - docker push $APP_IMAGE_DEBUG
    - docker push $NGINX_IMAGE_DEBUG
    - docker push $POSTGRES_IMAGE_DEBUG
    - docker push $PRERENDER_IMAGE_DEBUG
  except:
    - schedules

run-tests:
  stage: test
  image: $BUILDER_IMAGE
  before_script:
    - export APP_IMAGE=$(resolve_nested.sh APP_IMAGE_DEBUG) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export NGINX_IMAGE=$(resolve_nested.sh NGINX_IMAGE_DEBUG) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export POSTGRES_IMAGE=$(resolve_nested.sh POSTGRES_IMAGE_DEBUG) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export PRERENDER_IMAGE=$(resolve_nested.sh PRERENDER_IMAGE_DEBUG) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - cd $CI_PROJECT_DIR
    - echo $DOCKER_PW | docker login -u $DOCKER_USER --password-stdin $DOCKER_REGISTRY
    - mv "${BUILDER_NODE_MODULES_SRC_DIRECTORY}" "${BUILDER_NODE_MODULES_DESTINATION_DIRECTORY}"
    - . "${BUILDER_VENV_DIRECTORY}/bin/activate"
    - docker-compose up -d
    - cd "${CLIENT_DIRECTORY}"
    - sleep 20
  script:
    - npm run test-ci || (cat /root/.npm/_logs/*; exit 1)
  artifacts:
    paths:
      - $CI_PROJECT_DIR/client/cypress/screenshots/*
    expire_in: 1 week
    when: on_failure
  except:
    - schedules

test-release:
  stage: release
  image: $BUILDER_IMAGE
  variables:
    VUE_DEBUG: "true"
    DJANGO_DEBUG: "True"
    NGINX_PRODUCTION_CONFIG: "True"
    # PRODUCTION_URL: "https://${TEST_PRODUCTION_DOMAIN}/"
    NGINX_SERVER_NAME: "${TEST_PRODUCTION_DOMAIN}"
    VIRTUAL_HOST: "${TEST_PRODUCTION_DOMAIN},www.${TEST_PRODUCTION_DOMAIN}"
    DJANGO_ALLOWED_HOST_1: "${TEST_PRODUCTION_DOMAIN}"
    DJANGO_ALLOWED_HOST_2: "www.${TEST_PRODUCTION_DOMAIN}"
    # NGINX_PERMANENT_REDIRECT_INSTRUCTION: "return 301 ${PRODUCTION_URL}$$request_uri;"
    DJANGO_DATABASE_PASSWORD: "${DJANGO_DATABASE_PASSWORD_TEST}"
    DJANGO_SECRET_KEY: "${DJANGO_SECRET_KEY_TEST}"
    HOST_PORT: 80
    TEST_DEPLOYMENT: "True"
  before_script:
    - cd $CI_PROJECT_DIR
    - echo $DOCKER_PW | docker login -u $DOCKER_USER --password-stdin $DOCKER_REGISTRY
    - mv "${BUILDER_NODE_MODULES_SRC_DIRECTORY}" "${BUILDER_NODE_MODULES_DESTINATION_DIRECTORY}"
    - export PRODUCTION_URL="https://${TEST_PRODUCTION_DOMAIN}/"
    - export APP_IMAGE_NAME=$(resolve_nested.sh APP_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export NGINX_IMAGE_NAME=$(resolve_nested.sh NGINX_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export TEST_PRODUCTION_DOMAIN_ENCRYPTED_USER_PASSWORD=$(htpasswd -nb ${TEST_PRODUCTION_DOMAIN_USER} ${TEST_PRODUCTION_DOMAIN_PASSWORD})
    - export POSTGRES_IMAGE_NAME=$(resolve_nested.sh POSTGRES_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export PRERENDER_IMAGE_NAME=$(resolve_nested.sh PRERENDER_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    # - export NGINX_PERMANENT_REDIRECT_INSTRUCTION="return 301 ${PRODUCTION_URL}$$request_uri;"
    - export NGINX_BASIC_AUTH_FOR_TEST_DOMAIN="auth_basic \"Testing instances of our websites need authentication :P\"; auth_basic_user_file /etc/apache2/.htpasswd;" # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - echo "Building for production..."
  script:
    - ./build_gitlab.sh
    - docker build
      --build-arg CLIENT_DIRECTORY
      --build-arg BACKEND_DIRECTORY
      --build-arg APP_IMAGE_BACKEND_DIRECTORY
      --build-arg APP_INDEX_HTML_TEMPLATE_DIRECTORY
      --build-arg DJANGO_SECRET_KEY
      --build-arg DJANGO_DEBUG
      --build-arg DJANGO_DATABASE_NAME
      --build-arg DJANGO_DATABASE_USERNAME
      --build-arg DJANGO_DATABASE_PASSWORD
      --build-arg DJANGO_DATABASE_PORT
      --build-arg DJANGO_DATABASE_HOST
      --build-arg DJANGO_ALLOWED_HOST_1
      --build-arg DJANGO_ALLOWED_HOST_2
      --build-arg DJANGO_ALLOWED_HOST_3
      --build-arg DJANGO_ALLOWED_HOST_4
      --build-arg DJANGO_STATICFILES_RELATIVE_TO_BACKEND_DIRECTORY
      --build-arg DJANGO_STATIC_ROOT_RELATIVE_TO_BACKEND_DIRECTORY
      --build-arg DJANGO_MEDIA_ROOT_RELATIVE_TO_BACKEND_DIRECTORY
      --build-arg DJANGO_STATIC_URL
      --build-arg DJANGO_MEDIA_URL
      --build-arg GUNICORN_PORT
      -t $APP_IMAGE_NAME .
    - docker build
      --build-arg NGINX_IMAGE_STATICFILES_DESTINATION_DIRECTORY
      --build-arg NGINX_IMAGE_MEDIAFILES_DESTINATION_DIRECTORY
      --build-arg CERTBOT_EMAIL
      --build-arg GUNICORN_PORT
      --build-arg NGINX_SERVER_NAME
      --build-arg NGINX_PERMANENT_REDIRECT_INSTRUCTION
      --build-arg NGINX_BASIC_AUTH_FOR_TEST_DOMAIN
      --build-arg NGINX_PRODUCTION_CONFIG
      --build-arg VIRTUAL_HOST
      --build-arg TEST_PRODUCTION_DOMAIN_ENCRYPTED_USER_PASSWORD
      -t $NGINX_IMAGE_NAME -f nginx/Dockerfile .
    - docker build
      --build-arg DJANGO_DATABASE_NAME
      --build-arg DJANGO_DATABASE_USERNAME
      --build-arg DJANGO_DATABASE_PASSWORD
      -t $POSTGRES_IMAGE_NAME -f postgres/Dockerfile .
    - docker build
      -t $PRERENDER_IMAGE_NAME -f prerender/Dockerfile .
  after_script:
    - export APP_IMAGE_NAME=$(resolve_nested.sh APP_IMAGE_PRODUCTION_TEST) # It seems like exported variables from the before_script only stay in the before_script and script, but not in after_script.
    - export NGINX_IMAGE_NAME=$(resolve_nested.sh NGINX_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export POSTGRES_IMAGE_NAME=$(resolve_nested.sh POSTGRES_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export PRERENDER_IMAGE_NAME=$(resolve_nested.sh PRERENDER_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - docker push $APP_IMAGE_NAME
    - docker push $NGINX_IMAGE_NAME
    - docker push $POSTGRES_IMAGE_NAME
    - docker push $PRERENDER_IMAGE_NAME
    - chmod 755 util/scp_ssh_wrapper.sh
    - source util/scp_ssh_wrapper.sh
    - scp_proxy "${BACKEND_DIRECTORY}/coverage.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/" || echo "Could not upload coverage report to server. Maybe your app has not been deployed yet? Not considering this as an error."
  only:
    - tags

test-deploy:
  stage: deploy
  image: $BUILDER_IMAGE
  variables:
    NGINX_SERVER_NAME: "${TEST_PRODUCTION_DOMAIN}"
    TEST_DEPLOYMENT: "True"
  before_script:
    - export APP_IMAGE_NAME=$(resolve_nested.sh APP_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export NGINX_IMAGE_NAME=$(resolve_nested.sh NGINX_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export POSTGRES_IMAGE_NAME=$(resolve_nested.sh POSTGRES_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export PRERENDER_IMAGE_NAME=$(resolve_nested.sh PRERENDER_IMAGE_PRODUCTION_TEST) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export PRODUCTION_URL="https://${TEST_PRODUCTION_DOMAIN}/" # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - cd $CI_PROJECT_DIR
    - chmod 755 deploy/deploy.sh
  script:
    - echo "Deploying to test production..."
    - deploy/deploy.sh
  only:
    - tags

release:
  stage: release
  image: $BUILDER_IMAGE
  variables:
    VUE_DEBUG: "false"
    DJANGO_DEBUG: "False"
    NGINX_PRODUCTION_CONFIG: "True"
    NGINX_SERVER_NAME: "${PRODUCTION_DOMAIN}"
    VIRTUAL_HOST: "${PRODUCTION_DOMAIN},www.${PRODUCTION_DOMAIN}"
    DJANGO_ALLOWED_HOST_1: "${PRODUCTION_DOMAIN}"
    DJANGO_ALLOWED_HOST_2: "www.${PRODUCTION_DOMAIN}"
    HOST_PORT: 80
  before_script:
    - cd $CI_PROJECT_DIR
    - echo $DOCKER_PW | docker login -u $DOCKER_USER --password-stdin $DOCKER_REGISTRY
    - mv "${BUILDER_NODE_MODULES_SRC_DIRECTORY}" "${BUILDER_NODE_MODULES_DESTINATION_DIRECTORY}"
    - export PRODUCTION_URL="https://${PRODUCTION_DOMAIN}/" # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export NGINX_PERMANENT_REDIRECT_INSTRUCTION="return 301 ${PRODUCTION_URL}$$request_uri;" # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    # - export NGINX_BASIC_AUTH_FOR_TEST_DOMAIN="auth_basic_user_file /etc/apache2/.htpasswd;" # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export APP_IMAGE_NAME=$(resolve_nested.sh APP_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export NGINX_IMAGE_NAME=$(resolve_nested.sh NGINX_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export POSTGRES_IMAGE_NAME=$(resolve_nested.sh POSTGRES_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export PRERENDER_IMAGE_NAME=$(resolve_nested.sh PRERENDER_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - echo "Building for production..."
  script:
    - ./build_gitlab.sh
    - docker build
      --build-arg CLIENT_DIRECTORY
      --build-arg BACKEND_DIRECTORY
      --build-arg APP_IMAGE_BACKEND_DIRECTORY
      --build-arg APP_INDEX_HTML_TEMPLATE_DIRECTORY
      --build-arg DJANGO_SECRET_KEY
      --build-arg DJANGO_DEBUG
      --build-arg DJANGO_DATABASE_NAME
      --build-arg DJANGO_DATABASE_USERNAME
      --build-arg DJANGO_DATABASE_PASSWORD
      --build-arg DJANGO_DATABASE_PORT
      --build-arg DJANGO_DATABASE_HOST
      --build-arg DJANGO_ALLOWED_HOST_1
      --build-arg DJANGO_ALLOWED_HOST_2
      --build-arg DJANGO_ALLOWED_HOST_3
      --build-arg DJANGO_ALLOWED_HOST_4
      --build-arg DJANGO_STATICFILES_RELATIVE_TO_BACKEND_DIRECTORY
      --build-arg DJANGO_STATIC_ROOT_RELATIVE_TO_BACKEND_DIRECTORY
      --build-arg DJANGO_MEDIA_ROOT_RELATIVE_TO_BACKEND_DIRECTORY
      --build-arg DJANGO_STATIC_URL
      --build-arg DJANGO_MEDIA_URL
      --build-arg GUNICORN_PORT
      -t $APP_IMAGE_NAME .
    - docker build
      --build-arg NGINX_IMAGE_STATICFILES_DESTINATION_DIRECTORY
      --build-arg NGINX_IMAGE_MEDIAFILES_DESTINATION_DIRECTORY
      --build-arg CERTBOT_EMAIL
      --build-arg GUNICORN_PORT
      --build-arg NGINX_SERVER_NAME
      --build-arg NGINX_PERMANENT_REDIRECT_INSTRUCTION
      --build-arg NGINX_PRODUCTION_CONFIG
      --build-arg VIRTUAL_HOST
      -t $NGINX_IMAGE_NAME -f nginx/Dockerfile .
    - docker build
      --build-arg DJANGO_DATABASE_NAME
      --build-arg DJANGO_DATABASE_USERNAME
      --build-arg DJANGO_DATABASE_PASSWORD
      -t $POSTGRES_IMAGE_NAME -f postgres/Dockerfile .
    - docker build
      -t $PRERENDER_IMAGE_NAME -f prerender/Dockerfile .
  after_script:
    - export APP_IMAGE_NAME=$(resolve_nested.sh APP_IMAGE_PRODUCTION) # It seems like exported variables from the before_script only stay in the before_script and script, but not in after_script.
    - export NGINX_IMAGE_NAME=$(resolve_nested.sh NGINX_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export POSTGRES_IMAGE_NAME=$(resolve_nested.sh POSTGRES_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export PRERENDER_IMAGE_NAME=$(resolve_nested.sh PRERENDER_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - docker push $APP_IMAGE_NAME
    - docker push $NGINX_IMAGE_NAME
    - docker push $POSTGRES_IMAGE_NAME
    - docker push $PRERENDER_IMAGE_NAME
  only:
    - tags

deploy:
  stage: deploy
  image: $BUILDER_IMAGE
  variables:
    NGINX_SERVER_NAME: "${PRODUCTION_DOMAIN}"
  before_script:
    - export APP_IMAGE_NAME=$(resolve_nested.sh APP_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export NGINX_IMAGE_NAME=$(resolve_nested.sh NGINX_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export POSTGRES_IMAGE_NAME=$(resolve_nested.sh POSTGRES_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export PRERENDER_IMAGE_NAME=$(resolve_nested.sh PRERENDER_IMAGE_PRODUCTION) # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - export PRODUCTION_URL="https://${PRODUCTION_DOMAIN}/" # If nested variables are not used directly in .gitlab-ci.yml, they will not be substituted completely. Workaround: USE them in .gitlab-ci.yml by this command, instead of just defining a new variable in the variables section of the job (where no "usage" happens, thus no substitution)
    - cd $CI_PROJECT_DIR
    - chmod 755 deploy/deploy.sh
  script:
    - echo "Deploying to production..."
    - deploy/deploy.sh
  only:
    - tags
  when: manual
  allow_failure: false

create_badges:
  stage: analyze
  image: $BUILDER_IMAGE
  variables:
    NGINX_SERVER_NAME: "${TEST_PRODUCTION_DOMAIN}"
    TEST_DEPLOYMENT: "True" # The badges should always go to the testserver, and if the test-server variables are not defined, this has no effects.
  before_script:
    - echo $DOCKER_PW | docker login -u $DOCKER_USER --password-stdin $DOCKER_REGISTRY
    - chmod 755 util/run_lighthouse.sh
    - chmod 755 util/update_dependency_badges.sh
    - mv "${BUILDER_NODE_MODULES_SRC_DIRECTORY}" "${BUILDER_NODE_MODULES_DESTINATION_DIRECTORY}"
  script:
    - sleep 60 # To give nginx some time to serve the site.
    - util/run_lighthouse.sh
  after_script:
    - chmod 755 util/scp_ssh_wrapper.sh
    - source util/scp_ssh_wrapper.sh
    - scp_proxy "report.html" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "latest_release_tag.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "nginx_version.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "postgres_version.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "webpack_version.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "vue_version.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "django_version.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "performance.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "accessibility.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "best_practices.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "search_engine_results_ranking.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "outdated.html" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "outdated_pip.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "outdated_npm.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
  only:
    - tags

update_dependency_badges:
  stage: analyze
  image: $BUILDER_IMAGE
  variables:
    NGINX_SERVER_NAME: "${TEST_PRODUCTION_DOMAIN}"
    TEST_DEPLOYMENT: "True" # The badges should always go to the testserver, and if the test-server variables are not defined, this has no effects.
  before_script:
    - echo $DOCKER_PW | docker login -u $DOCKER_USER --password-stdin $DOCKER_REGISTRY
    - chmod 755 util/update_dependency_badges.sh
    - mv "${BUILDER_NODE_MODULES_SRC_DIRECTORY}" "${BUILDER_NODE_MODULES_DESTINATION_DIRECTORY}"
  script:
    - sleep 60 # To give nginx some time to serve the site.
    - util/update_dependency_badges.sh
  after_script:
    - chmod 755 util/scp_ssh_wrapper.sh
    - source util/scp_ssh_wrapper.sh
    - scp_proxy "outdated.html" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "outdated_pip.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
    - scp_proxy "outdated_npm.svg" "~/apps/${NGINX_SERVER_NAME}/gitlab-badges/"
  only:
    - schedules
