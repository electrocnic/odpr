from django.apps import AppConfig


class OdprSharedModelsConfig(AppConfig):
    name = 'odpr_shared_models'
