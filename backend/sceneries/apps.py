from django.apps import AppConfig


class SceneriesConfig(AppConfig):
    name = 'sceneries'
