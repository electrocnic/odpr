# Generated by Django 2.2.5 on 2019-09-16 19:47

import annoying.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import filer.fields.file
import filer.fields.image


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('filer', '0012_auto_20190916_2144'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('odpr_shared_models', '0001_initial'),
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('title', models.CharField(max_length=200)),
                ('visit_count', models.IntegerField(default=0, verbose_name='Viewed')),
                ('privacy_setting', models.IntegerField(choices=[(0, 'Public'), (1, 'Link'), (2, 'Staff'), (3, 'Private')], default=0)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='documents', to=settings.AUTH_USER_MODEL)),
                ('description', annoying.fields.AutoOneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, to='odpr_shared_models.TextWithHistory')),
            ],
        ),
        migrations.CreateModel(
            name='DocumentationTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('tag', models.CharField(max_length=45, unique=True)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='documentation_tags', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DocumentationImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='uploaded_documentation_images', to=settings.AUTH_USER_MODEL)),
                ('document', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='document_images', to='documentation.Document')),
                ('image', filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='documentation_images', to=settings.FILER_IMAGE_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Documentation Images',
            },
        ),
        migrations.CreateModel(
            name='DocumentationFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='uploaded_documentation_files', to=settings.AUTH_USER_MODEL)),
                ('document', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='documentation_files', to='documentation.Document')),
                ('file', filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='documentation_files', to='filer.File')),
            ],
            options={
                'verbose_name_plural': 'Documentation Files',
            },
        ),
        migrations.AddField(
            model_name='document',
            name='tags',
            field=models.ManyToManyField(blank=True, related_name='tagged_documents', to='documentation.DocumentationTag'),
        ),
    ]
