from django.apps import AppConfig


class SceneryRequestsConfig(AppConfig):
    name = 'scenery_requests'
